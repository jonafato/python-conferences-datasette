from pathlib import Path
import sys

from click.testing import CliRunner
from sqlite_utils.cli import insert


def main(data_path: str) -> None:
    runner = CliRunner()
    for path in Path(data_path).glob("*.csv"):
        runner.invoke(
            insert,
            [
                "conferences.db",
                "conferences",
                str(path.absolute()),
                "--csv",
                "--convert",
                f"row['Year'] = {path.stem}",
                "--pk",
                "Year",
                "--pk",
                "Subject",
                "--replace",
                "--detect-types",
            ]
        )


if __name__ == "__main__":
    main(data_path=sys.argv[1])
